package com.cormacx.deathlink.deathlinkservice.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.Period;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import com.cormacx.deathlink.deathlinkservice.model.death.Death;
import com.cormacx.deathlink.deathlinkservice.model.person.Person;

@SpringBootTest
public class DeathRepositoryTest {
	
	@Autowired
	DeathRepository deathRepo;
	
	@Test
	public void saveDeathWithRequiredFields_savesSuccessfully() {
		Person person = new Person();
		person.setProfession("Lenhador");
		person.setName("Jos� da Silva");
		person.setDadsName("Antonio da Silva");
		person.setMomsName("Maria Aparecida da Silva");
		person.setIsDeceased(true);
		LocalDate birthday = LocalDate.of(1960, 11, 22);
		person.setBirthday(birthday);
		person.setAgeInYears(Period.between(birthday, LocalDate.now()).getYears());
		Death death = new Death();
		death.setDateOfDeath(LocalDate.now());
		death.setDateOfBurial(LocalDate.now().plusDays(1L));
		death.setFunerary("Funer�ria Dois Irm�os");
		death.setPlaceOfDeath("Hospital Nossa Senhora Aparecida");
		death.setPlaceOfBurial("Cemit�rio �gua Verde");
		death.setPlaceOfFuneral("Cemit�rio �gua Verde - Capela 1");
		death.setPerson(person);
		
		Death savedDeath = deathRepo.save(death);
		
		assertThat(savedDeath.getId() != null);
		assertThat(savedDeath.getDateOfBurial().equals(death.getDateOfBurial()));
		assertThat(savedDeath.getDateOfDeath().equals(death.getDateOfDeath()));
		assertThat(savedDeath.getPerson().equals(death.getPerson()));
		assertThat(savedDeath.getFunerary().equals(death.getFunerary()));
		assertThat(savedDeath.getPlaceOfBurial().equals(death.getPlaceOfBurial()));
		assertThat(savedDeath.getPlaceOfDeath().equals(death.getPlaceOfDeath()));
		assertThat(savedDeath.getPlaceOfFuneral().equals(death.getPlaceOfFuneral()));
		
		deathRepo.delete(savedDeath);
	}
	
	@Test
	public void saveDeathWithoutPerson_throwsException() {
		Death death = new Death();
		death.setDateOfBurial(LocalDate.now());
		death.setDateOfDeath(LocalDate.now());
		death.setFunerary("");
		death.setPlaceOfBurial("test test test");
		death.setPlaceOfDeath("test test test");
		death.setPlaceOfFuneral("test test test");
		
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
			deathRepo.save(death);
		});
	}
	
	@Test
	public void saveDeathWithoutRequiredDateOfDeath_throwsException() {
		Death death = new Death();
		death.setDateOfBurial(LocalDate.now());
		death.setFunerary("");
		death.setPlaceOfBurial("test test test");
		death.setPlaceOfDeath("test test test");
		death.setPlaceOfFuneral("test test test");
	}

}
