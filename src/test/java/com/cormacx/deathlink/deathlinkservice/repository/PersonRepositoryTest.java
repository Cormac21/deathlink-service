package com.cormacx.deathlink.deathlinkservice.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import com.cormacx.deathlink.deathlinkservice.model.person.Person;

@SpringBootTest
public class PersonRepositoryTest {

	@Autowired
	PersonRepository personRepo;
	
	@Test
	public void savePersonWithRequiredFields_savesSuccessfully() {
		Person person = new Person();
		person.setName("Jos� da Silva");
		
		Person savedPerson = personRepo.save(person);
		assertThat(savedPerson.getName().equals(person.getName()));
		assertThat(savedPerson.getId()).isNotNull();
	}
	
	@Test
	public void savePersonWithoutName_throwsException() {
		Person person = new Person();
		
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
			personRepo.save(person);
		});
	}
	
	@Test
	public void savePersonWithNameSpecialCharacters_throwsException() {
		
	}
	
}
