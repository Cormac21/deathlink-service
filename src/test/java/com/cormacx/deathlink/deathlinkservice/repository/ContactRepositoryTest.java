package com.cormacx.deathlink.deathlinkservice.repository;

import static org.assertj.core.api.Assertions.assertThat;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import com.cormacx.deathlink.deathlinkservice.model.Contact;
import com.cormacx.deathlink.deathlinkservice.model.user.User;

@SpringBootTest
public class ContactRepositoryTest {
	
	@Autowired
	ContactRepository contactRepository;
	
	User user = new User(1L, "cormacx", "psswrd", "test@test.com", true, true, true, true);
	
	@Test
	public void saveContactWithInvalidEmail_throwsException() {
		Contact contact = new Contact();
		contact.setEmail("lwkregowergodfngldknfgkhgkshfgosflsslflslsfnkdn");
		
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			contactRepository.save(contact);
		});
	}
	
	@Test
	public void saveContactWithoutAnyInformation_doesntSave() {
		Contact contact = new Contact();
		contact.setName("");
		contact.setEmail("");
		contact.setPhoneNumber("");
		contact.setType("");
		
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
			contactRepository.save(contact);
		});
	}

	@Test
	public void saveContactWithInvalidPhoneNumber_throwsException() {
		Contact contact = new Contact();
		contact.setPhoneNumber("dkljkfngkdfghkfgkdjf");
		
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			contactRepository.save(contact);
		});
	}
	
	@Test
	public void saveContactWithNoNameButAnEmail_savesSuccessfully() {
		Contact contact = new Contact();
		contact.setEmail("valid.email@test.com");
		contact.setName("");
		contact.setUser(user);
		
		Contact savedContact = contactRepository.save(contact);
		assertThat(savedContact.getEmail().equals(savedContact.getEmail()));
		assertThat(savedContact.getUser().equals(savedContact.getUser()));
		assertThat(savedContact.getName().isEmpty());
		
		contactRepository.delete(savedContact);
	}
	
}
