package com.cormacx.deathlink.deathlinkservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class HelloResourceTest {

	@Autowired
	MockMvc mockMvc;
	
	@Test
	@Disabled
	public void hello_returns200_whenAccessed() throws Exception {
		mockMvc.perform(get("/hello"))
			.andExpect(status().is2xxSuccessful());
	}
	
}
