package com.cormacx.deathlink.deathlinkservice.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.cormacx.deathlink.deathlinkservice.model.user.User;
import com.cormacx.deathlink.deathlinkservice.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	UserService userService;

	@BeforeEach
	public void init() {
		User user = new User(1L, "cormacx", "psswrd", "test@test.com", true, true, true, true);

		Mockito.lenient().when(userRepository.findByEmail("test@test.com")).thenReturn(Optional.of(user));
	}
	
	@Test
	public void userService_calls_userRepository_atLeastOnceWhileGettingUsername() {
		userService.loadUserByUsername("test@test.com");
		
		verify(userRepository, times(1)).findByEmail("test@test.com");
	}
	
	@Test
	public void userService_throwsUsernameNotFoundException_forUnknownEmail() {
		Assertions.assertThrows(UsernameNotFoundException.class, () -> {
			userService.loadUserByUsername("lskdngfwskdjfoweikj");
		});
	}
	
	@Test
	public void userService_throwsException_forEmptyEmail() {
		Assertions.assertThrows(UsernameNotFoundException.class, () -> {
			userService.loadUserByUsername(null);
		});
	}
	
}
