package com.cormacx.deathlink.deathlinkservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.cormacx.deathlink.deathlinkservice.exception.DuplicatePersonException;
import com.cormacx.deathlink.deathlinkservice.model.person.Person;
import com.cormacx.deathlink.deathlinkservice.repository.PersonRepository;

@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {
	
	@Mock
	PersonRepository personRepo;
	
	@InjectMocks
	PersonService personService;
	
	@Test
	public void savePersonWithRequiredFields_savesSuccessfully() {
		Person person = new Person();
		person.setName("Jos� da Silva");
		person.setBirthday(LocalDate.of(1978, 12, 14));
		when(personRepo.save(any(Person.class))).thenReturn(person);
		
		Person saved = personService.savePerson(person);
		
		assertThat(saved.getName().equals(person.getName()));
		assertThat(saved.getBirthday().equals(person.getBirthday()));
	}
	
	@Test
	public void savePersonWithoutName_throwsException() {
		Person person = new Person();
		person.setBirthday(LocalDate.of(1978, 12, 14));
		
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			personService.savePerson(person);
		});
	}
	
	@Test
	public void savePersonWithSameName_throwsException() {
		Person person = new Person();
		person.setName("Jos� da Silva");
		List<Person> expectedSavedList = new ArrayList<Person>();
		expectedSavedList.add(person);
		
		when(personRepo.findAllByName("Jos� da Silva")).thenReturn(expectedSavedList);
		
		Assertions.assertThrows(DuplicatePersonException.class, () -> {
			personService.savePerson(person);
		});
	}
	
	@Test
	public void personExists_personDoesNotExist_returnsFalse() {
		Person crazyEight = new Person();
		crazyEight.setName("Crazy Eight");
		when(personRepo.findAllByName("Crazy Eight")).thenReturn(Collections.emptyList());
		
		boolean result = personService.personExists(crazyEight);
		assertThat(result).isFalse();
	}
	
	@Test
	public void personExists_personDoesExist_AndIsSamePerson_returnsTrue() {
		Person crazyEight = new Person();
		crazyEight.setName("Crazy Eight");
		crazyEight.setBirthday(LocalDate.of(1993, 8, 12));
		crazyEight.setMomsName("Crazy Eight's mom");
		List<Person> expectedSavedList = new ArrayList<Person>();
		expectedSavedList.add(crazyEight);
		
		when(personRepo.findAllByName("Crazy Eight")).thenReturn(expectedSavedList);
		
		boolean result = personService.personExists(crazyEight);
		assertThat(result).isTrue();
	}
	
	@Test
	public void personExists_personDoesExist_ButIsSlightlyDifferent_returnsTrue() {
		Person crazyEight = new Person();
		crazyEight.setName("Crazy Eight");
		crazyEight.setBirthday(LocalDate.of(1993, 8, 12));
		crazyEight.setMomsName("Crazy Eight's mom");
		crazyEight.setDadsName("Crazy Eight's dad");
		List<Person> expectedSavedList = new ArrayList<Person>();
		expectedSavedList.add(crazyEight);
		
		when(personRepo.findAllByName("Crazy Eight")).thenReturn(expectedSavedList);
		
		Person crazyEight2 = new Person();
		crazyEight2.setName("Crazy Eight");
		crazyEight2.setBirthday(LocalDate.of(1993, 8, 12));
		crazyEight2.setMomsName("Crazy Eight's mom");
		crazyEight2.setDadsName("Crazy Eight's dad");
		crazyEight2.setIsDeceased(true);
		
		boolean result = personService.personExists(crazyEight2);
		assertThat(result).isTrue();
	}
	
	@Test
	public void personExists_personDoesExist_ButHasDifferentBirthday() {
		Person crazyEight = new Person();
		crazyEight.setName("Crazy Eight");
		crazyEight.setBirthday(LocalDate.of(1993, 8, 12));
		List<Person> expectedSavedList = new ArrayList<Person>();
		expectedSavedList.add(crazyEight);
		
		when(personRepo.findAllByName("Crazy Eight")).thenReturn(expectedSavedList);
		
		Person crazyEight2 = new Person();
		crazyEight2.setName("Crazy Eight");
		crazyEight2.setBirthday(LocalDate.of(1953, 8, 12));
		
		boolean result = personService.personExists(crazyEight2);
		assertThat(result).isFalse();
	}
	
	@Test
	public void personExists_personDoesExist_ButHasDifferentMother() {
		Person crazyEight = new Person();
		crazyEight.setName("Crazy Eight");
		crazyEight.setMomsName("Crazy Eight's mom");
		List<Person> expectedSavedList = new ArrayList<Person>();
		expectedSavedList.add(crazyEight);
		
		when(personRepo.findAllByName("Crazy Eight")).thenReturn(expectedSavedList);
		
		Person crazyEight2 = new Person();
		crazyEight2.setName("Crazy Eight");
		crazyEight2.setMomsName("Some other motherly name");
		
		boolean result = personService.personExists(crazyEight2);
		assertThat(result).isFalse();
	}

}
