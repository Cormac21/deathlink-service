package com.cormacx.deathlink.deathlinkservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.cormacx.deathlink.deathlinkservice.model.Contact;
import com.cormacx.deathlink.deathlinkservice.model.user.User;
import com.cormacx.deathlink.deathlinkservice.repository.ContactRepository;

@ExtendWith(MockitoExtension.class)
public class ContactServiceTest {

	@Mock
	ContactRepository contactRepository;
	
	@InjectMocks
	ContactService contactService;
	
	User user = new User(1L, "cormacx", "psswrd", "test@test.com", true, true, true, true);
	Contact contac = new Contact(1L, "Seu z� do caix�o", "email", null, "seu.ze.do.caixao@test.com", user, false);
	
	@BeforeEach
	public void init() {
		List<Contact> contacts = new ArrayList<Contact>();
		contacts.add(contac);
		
		Mockito.lenient().when(contactRepository.findAllByUserEmail("test@test.com")).thenReturn(contacts);
		Mockito.lenient().when(contactRepository.findAllByUserEmail("wdjkhnfkiwsjdgh")).thenReturn(Collections.emptyList());
	}
	
	@Test
	public void findContactsForUser_returnsEmpty_forUnknownUser() {
		List<Contact> contacts = contactService.findContactsForUser("wdjkhnfkiwsjdgh");
		assertThat(contacts.isEmpty());
	}
	
	@Test
	public void findContactsForUser_returnsContact_forKnownUser() {
		List<Contact> contacts = contactService.findContactsForUser("test@test.com");
		assertThat(contacts.size() == 1);
	}
	
	@Test
	public void saveContactForUser_returnsEntity_forKnownUser() {
		
		when(contactRepository.save(any(Contact.class))).thenReturn(contac);
		
		Contact newContact = new Contact();
		newContact.setEmail("test.email@email.com");
		newContact.setUser(user);
		Contact savedContact = contactService.saveContactForUser(newContact);
		
		assertThat(savedContact.getEmail()).isSameAs(newContact.getEmail());
	}
	
}
