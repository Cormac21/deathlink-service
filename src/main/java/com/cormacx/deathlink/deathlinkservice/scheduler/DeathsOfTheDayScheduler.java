package com.cormacx.deathlink.deathlinkservice.scheduler;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

@Component
@ConditionalOnProperty(value = "false")
public class DeathsOfTheDayScheduler {

	private static final Logger log = LoggerFactory.getLogger(DeathsOfTheDayScheduler.class);
	
	private static final String URL = "https://obituarios.curitiba.pr.gov.br/publico/falecimentos.aspx";
	
	
	@Scheduled(fixedRate = 1000000000)
	public void goToMunicipalSiteAndGetDeaths() {
		
		WebClient client = new WebClient();
		client.getOptions().setJavaScriptEnabled(false);
		client.getOptions().setCssEnabled(false);
		client.getOptions().setUseInsecureSSL(true);
		
		try {
			HtmlPage page = client.getPage(URL);
			List<HtmlElement> elements = page.getByXPath("//tbody");
			System.out.println(page.asXml());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
