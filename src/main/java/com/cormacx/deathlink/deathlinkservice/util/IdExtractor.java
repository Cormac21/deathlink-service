package com.cormacx.deathlink.deathlinkservice.util;

import org.springframework.stereotype.Component;

@Component
public class IdExtractor {

	public String extrairIdDeLink( String link ) {
		return link.substring(link.lastIndexOf('/')+1, link.length());
	}
	
}
