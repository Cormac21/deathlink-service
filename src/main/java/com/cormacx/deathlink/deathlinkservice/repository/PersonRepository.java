package com.cormacx.deathlink.deathlinkservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cormacx.deathlink.deathlinkservice.model.person.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long>{

	List<Person> findAllByName( String name );
	
}
