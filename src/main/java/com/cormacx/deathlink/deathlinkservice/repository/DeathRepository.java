package com.cormacx.deathlink.deathlinkservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cormacx.deathlink.deathlinkservice.model.death.Death;

@Repository
public interface DeathRepository extends JpaRepository<Death, Long>{

	
	
}
