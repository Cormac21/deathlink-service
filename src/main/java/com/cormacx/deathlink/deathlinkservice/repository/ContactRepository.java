package com.cormacx.deathlink.deathlinkservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cormacx.deathlink.deathlinkservice.model.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long>{

	List<Contact> findAllByUserId(Long userId);
	
	List<Contact> findAllByUserEmail( String username );
	
	Optional<Contact> findOptionalByIdAndUserId(Long id, Long userId);
	
}
