package com.cormacx.deathlink.deathlinkservice.exception;

public class DuplicatePersonException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicatePersonException( String msg ) {
		super(msg);
	}
	
}
