package com.cormacx.deathlink.deathlinkservice.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cormacx.deathlink.deathlinkservice.handler.DeathModelHandler;
import com.cormacx.deathlink.deathlinkservice.model.death.Death;
import com.cormacx.deathlink.deathlinkservice.resource.DeathModel;
import com.cormacx.deathlink.deathlinkservice.service.DeathService;

@RestController
public class DeathController {

	@Autowired
	private DeathService deathService;
	
	@Autowired
	private DeathModelHandler deathHandler;
	
	@GetMapping(path = "/deaths", produces = MediaTypes.COLLECTION_JSON_VALUE)
	public CollectionModel<DeathModel> findDeaths(@RequestParam(defaultValue = "0") Integer pageNumber, 
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(required = false) String sortBy ) {
		Pageable pageable = null;
		if ( !StringUtils.isEmpty(sortBy)) {
			pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));
		} else {
			pageable = PageRequest.of(pageNumber, pageSize);
		}
		Page<Death> deathPage = deathService.findAllDeaths(pageable);
		CollectionModel<DeathModel> deathModel = deathHandler.toCollectionModel(deathPage);
		deathModel.add(linkTo(methodOn(DeathController.class).findDeaths(pageNumber, pageSize, sortBy)).withSelfRel());
		return deathModel;
	}
	
	@GetMapping(path = "/deaths/{id}")
	public ResponseEntity<?> findDeathById( @PathVariable Long id ) {
		Optional<Death> death = deathService.findDeathById(id);
		if ( death.isPresent()) {
			return ResponseEntity.ok(deathHandler.toModel(death.get()));
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
}
