package com.cormacx.deathlink.deathlinkservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cormacx.deathlink.deathlinkservice.model.authentication.AuthenticationRequest;
import com.cormacx.deathlink.deathlinkservice.model.authentication.AuthenticationResponse;
import com.cormacx.deathlink.deathlinkservice.service.UserService;
import com.cormacx.deathlink.deathlinkservice.util.JwtUtil;

@RestController
public class HelloResource {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserService userDetailsService;
	
	@Autowired
	private JwtUtil jwtTokenUtil;

	@GetMapping(path = "/hello")
	public String hello() {
		return "Hello World";
	}
	
	@PostMapping(value = "/authenticate")
	public ResponseEntity<?> authenticate( @RequestBody AuthenticationRequest authenticationRequest ) {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
					);			
		} catch (BadCredentialsException e) {
			throw new BadCredentialsException("Incorrect username or password combination!", e);
		}
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		final String jwt = jwtTokenUtil.generateToken(userDetails);
		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}
	
}
