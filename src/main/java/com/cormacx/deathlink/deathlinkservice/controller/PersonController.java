package com.cormacx.deathlink.deathlinkservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cormacx.deathlink.deathlinkservice.handler.PersonModelHandler;
import com.cormacx.deathlink.deathlinkservice.model.person.Person;
import com.cormacx.deathlink.deathlinkservice.service.PersonService;

@RestController
public class PersonController {

	@Autowired
	private PersonService personService;
	
	@Autowired
	private PersonModelHandler personHandler;
	
	@GetMapping(path = "/persons/{id}")
	public ResponseEntity<?> findPersonById( @PathVariable Long id ) {
		Optional<Person> person = personService.findPersonById(id);
		if ( person.isPresent()) {
			return ResponseEntity.ok(personHandler.toModel(person.get()));
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
}
