package com.cormacx.deathlink.deathlinkservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cormacx.deathlink.deathlinkservice.handler.ContactHandler;
import com.cormacx.deathlink.deathlinkservice.model.Contact;
import com.cormacx.deathlink.deathlinkservice.model.user.UserPrincipal;
import com.cormacx.deathlink.deathlinkservice.resource.ContactModel;
import com.cormacx.deathlink.deathlinkservice.service.ContactService;
import com.cormacx.deathlink.deathlinkservice.service.UserService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class ContactController {
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private ContactHandler contactHandler;
	
	@Autowired
	private UserService userDetailsService;
	
	@GetMapping(path = "/contacts")
	public CollectionModel<ContactModel> findContactsByAuthenticatedUser(  ) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		List<Contact> userContacts = contactService.findContactsForUser(auth.getName());
		Link self = linkTo(methodOn(ContactController.class).findContactsByAuthenticatedUser()).withSelfRel(); 
		
		return contactHandler.toCollectionModel(userContacts).add(self);
	}
	
	@GetMapping("/contacts/{id}")
	public ResponseEntity<?> findContactByIdForAuthenticatedUser( @PathVariable Long id ) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserPrincipal user = (UserPrincipal) userDetailsService.loadUserByUsername(auth.getName());
		Optional<Contact> contact = contactService.findContactByIdAndUser( user.getUser().getId(), id );
		if( contact.isPresent() ) {
			return ResponseEntity.ok(contactHandler.toModel(contact.get()));
		} else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
	@PostMapping(path = "/contacts")
	public ResponseEntity<?> postNewContactForAuthenticatedUser( @RequestBody ContactModel contactModel ) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Contact contact = contactHandler.fromModel(contactModel);
		
		UserPrincipal user = (UserPrincipal) userDetailsService.loadUserByUsername(auth.getName());
		contact.setUser(user.getUser());
		contact = contactService.saveContactForUser(contact);
		return ResponseEntity.ok(contactHandler.toModel(contact));
	}
	
	@DeleteMapping("/contact")
	public ResponseEntity<?> deleteContactForAuthenticatedUser( @RequestBody ContactModel contactModel ) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Contact contact = contactHandler.fromModel(contactModel);
		
		UserPrincipal user = (UserPrincipal) userDetailsService.loadUserByUsername(auth.getName());
		contact.setUser(user.getUser());
		contactService.removeContact(contact);
		
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping("/contact/{contactId}")
	public ResponseEntity<?> deleteContactForAuthenticatedUserWithId( @PathVariable Long contactId ) {
		contactService.removeContactById(contactId);
		return ResponseEntity.ok().build();
	}

}
