package com.cormacx.deathlink.deathlinkservice.resource;

import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class ContactModel extends RepresentationModel<ContactModel>{
	
	private String name;
	
	private String type;
	
	private String phoneNumber;
	
	private String email;

	private boolean contactInCaseOfDeath;
	
}
