package com.cormacx.deathlink.deathlinkservice.resource;

import java.time.LocalDate;

import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PersonModel extends RepresentationModel<PersonModel>{
	
	private String name;
	
	private LocalDate birthday;
	
	private Integer ageInYears;
	
	private String profession;
	
	private String dadsName;
	
	private String momsName;
	
	private Boolean isDeceased;
	
	private Long deathId;
}
