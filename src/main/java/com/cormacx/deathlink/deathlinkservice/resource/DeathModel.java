package com.cormacx.deathlink.deathlinkservice.resource;

import java.time.LocalDate;

import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DeathModel extends RepresentationModel<DeathModel> {
	
	private LocalDate dateOfDeath;
	
	private String placeOfDeath;
	
	private String placeOfFuneral;
	
	private String placeOfBurial;
	
	private LocalDate dateOfBurial;
	
	private String funerary;
	
	private PersonModel person;

}
