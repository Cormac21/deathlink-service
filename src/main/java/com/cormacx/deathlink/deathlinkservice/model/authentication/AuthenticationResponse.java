package com.cormacx.deathlink.deathlinkservice.model.authentication;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class AuthenticationResponse {

	@Getter
	private final String jwt;
	
}
