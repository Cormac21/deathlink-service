package com.cormacx.deathlink.deathlinkservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.cormacx.deathlink.deathlinkservice.model.person.Person;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlanOfDeath {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_plan_of_death")
	private Long id;
	
	@Enumerated
	private TypeOfBodyHandling typeOfBodyHandling;
	
	private String placeOfRest;
	
	private String placeOfAshScattering;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "person_id_person")
	private Person person;
	
}
