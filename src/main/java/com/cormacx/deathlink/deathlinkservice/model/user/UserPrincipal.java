package com.cormacx.deathlink.deathlinkservice.model.user;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;

public class UserPrincipal extends org.springframework.security.core.userdetails.User {
	
	private static final long serialVersionUID = 1L;
	
	@Getter
	@JsonIgnore
	private final User user;

	public UserPrincipal(User user, Collection<? extends GrantedAuthority> authorities) {
		super(user.getEmail(), user.getPassword(), user.isEnabled(), user.isAccountNonExpired(), user.isCredentialsNonExpired(), user.isAccountNonLocked(), authorities);
		this.user = user;
	}

}
