package com.cormacx.deathlink.deathlinkservice.model.person;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.lang.NonNull;

import com.cormacx.deathlink.deathlinkservice.model.PlanOfDeath;
import com.cormacx.deathlink.deathlinkservice.model.death.Death;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_person")
	private Long id;
	
	@NonNull
	@Column(nullable = false)
	private String name;
	
	private LocalDate birthday;
	
	private Integer ageInYears;
	
	private String profession;
	
	private String dadsName;
	
	private String momsName;
	
	private Boolean isDeceased;
	
	@OneToOne(mappedBy = "person", fetch = FetchType.LAZY)
	private Death death;
	
	@OneToOne(mappedBy = "person", fetch = FetchType.LAZY)
	private PlanOfDeath planOfDeath;

	public Person(Long id, String name, LocalDate birthday, Integer ageInYears, String profession, String dadsName,
			String momsName, Boolean isDeceased) {
		super();
		this.id = id;
		this.name = name;
		this.birthday = birthday;
		this.ageInYears = ageInYears;
		this.profession = profession;
		this.dadsName = dadsName;
		this.momsName = momsName;
		this.isDeceased = isDeceased;
	}

}
