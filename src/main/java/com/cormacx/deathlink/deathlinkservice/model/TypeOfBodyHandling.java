package com.cormacx.deathlink.deathlinkservice.model;

public enum TypeOfBodyHandling {
	
	BURIAL("burial"), CREMATION("cremation"), NONE("none");
	
	String value;
	
	TypeOfBodyHandling(String string) {
		value = string;
	}

}
