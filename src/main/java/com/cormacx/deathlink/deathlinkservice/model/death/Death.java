package com.cormacx.deathlink.deathlinkservice.model.death;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.cormacx.deathlink.deathlinkservice.model.person.Person;

import lombok.Data;

@Data
@Entity
public class Death {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_death")
	private Long id;

	private LocalDate dateOfDeath;
	
	private String placeOfDeath;
	
	private String placeOfFuneral;
	
	private String placeOfBurial;
	
	private LocalDate dateOfBurial;
	
	private String funerary;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "person_id_person")
	private Person person;
	
}
