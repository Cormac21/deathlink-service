package com.cormacx.deathlink.deathlinkservice.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cormacx.deathlink.deathlinkservice.controller.DeathController;
import com.cormacx.deathlink.deathlinkservice.model.death.Death;
import com.cormacx.deathlink.deathlinkservice.resource.DeathModel;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class DeathModelHandler extends RepresentationModelAssemblerSupport<Death, DeathModel>{

	@Autowired
	PersonModelHandler personHandler;
	
	public DeathModelHandler() {
		super(DeathController.class, DeathModel.class);
	}

	@Override
	public DeathModel toModel(Death entity) {
		DeathModel model = super.instantiateModel(entity);
		model.setDateOfBurial(entity.getDateOfBurial());
		model.setDateOfDeath(entity.getDateOfDeath());
		model.setFunerary(entity.getFunerary());
		model.setPlaceOfBurial(entity.getPlaceOfBurial());
		model.setPlaceOfDeath(entity.getPlaceOfDeath());
		model.setPlaceOfFuneral(entity.getPlaceOfFuneral());
		model.add(linkTo(methodOn(DeathController.class).findDeathById(entity.getId())).withSelfRel());
		model.setPerson(personHandler.toModel(entity.getPerson()));
		return model;
	}
	
	public Death fromModel(DeathModel deathModel) {
		Death death = new Death();
		death.setDateOfBurial(deathModel.getDateOfBurial());
		death.setDateOfDeath(deathModel.getDateOfDeath());
		death.setFunerary(deathModel.getFunerary());
		death.setPlaceOfBurial(deathModel.getPlaceOfBurial());
		death.setPlaceOfDeath(deathModel.getPlaceOfDeath());
		death.setPlaceOfFuneral(deathModel.getPlaceOfFuneral());
		death.setPerson(personHandler.fromModel(deathModel.getPerson()));
		return death;
	}

}
