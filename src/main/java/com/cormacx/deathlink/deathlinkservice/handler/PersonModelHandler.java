package com.cormacx.deathlink.deathlinkservice.handler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cormacx.deathlink.deathlinkservice.controller.PersonController;
import com.cormacx.deathlink.deathlinkservice.model.person.Person;
import com.cormacx.deathlink.deathlinkservice.resource.PersonModel;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class PersonModelHandler extends RepresentationModelAssemblerSupport<Person, PersonModel>{

	public PersonModelHandler() {
		super(PersonController.class, PersonModel.class);
	}

	@Override
	public PersonModel toModel(Person entity) {
		PersonModel model = super.instantiateModel(entity);
		model.setName(entity.getName());
		model.setProfession(entity.getProfession());
		model.setBirthday(entity.getBirthday());
		model.setAgeInYears(entity.getAgeInYears());
		model.setDadsName(entity.getDadsName());
		model.setMomsName(entity.getMomsName());
		model.setIsDeceased(entity.getIsDeceased());
		model.setDeathId(entity.getDeath().getId());
		model.add(linkTo(methodOn(PersonController.class).findPersonById(entity.getId())).withSelfRel());
		return model;
	}

	public Person fromModel(PersonModel personModel) {
		Person person = new Person();
		person.setAgeInYears(personModel.getAgeInYears());
		person.setBirthday(personModel.getBirthday());
		person.setDadsName(personModel.getDadsName());
		person.setMomsName(personModel.getMomsName());
		person.setIsDeceased(personModel.getIsDeceased());
		person.setProfession(personModel.getProfession());
		person.setName(personModel.getName());
		//person.setDeath(personModel.getDeathId());
		
		return person;
	}
	
}
