package com.cormacx.deathlink.deathlinkservice.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cormacx.deathlink.deathlinkservice.controller.ContactController;
import com.cormacx.deathlink.deathlinkservice.model.Contact;
import com.cormacx.deathlink.deathlinkservice.resource.ContactModel;
import com.cormacx.deathlink.deathlinkservice.util.IdExtractor;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class ContactHandler extends RepresentationModelAssemblerSupport<Contact, ContactModel>{
	
	@Autowired
	IdExtractor idExtractor;
	
	public ContactHandler() {
		super(ContactController.class, ContactModel.class);
	}

	@Override
	public ContactModel toModel(Contact entity) {
		return ContactModel.builder()
				.name(entity.getName())
				.type(entity.getType())
				.email(entity.getEmail())
				.phoneNumber(entity.getPhoneNumber())
				.contactInCaseOfDeath(entity.isContactInCaseOfDeath())
				.build()
				.add(linkTo(methodOn(ContactController.class).findContactByIdForAuthenticatedUser(entity.getId())).withSelfRel());
	}
	
	public Contact fromIdModel( ContactModel model ) {
		return Contact.builder()
				.name(model.getName())
				.type(model.getType())
				.email(model.getEmail())
				.phoneNumber(model.getPhoneNumber())
				.id(Long.parseLong(idExtractor.extrairIdDeLink(model.getLink("self").toString())))
				.build();
	}
	
	public Contact fromModel( ContactModel model) {
		return Contact.builder()
				.name(model.getName())
				.type(model.getType())
				.email(model.getEmail())
				.phoneNumber(model.getPhoneNumber())
				.contactInCaseOfDeath(model.isContactInCaseOfDeath())
				.build();
	}

}
