package com.cormacx.deathlink.deathlinkservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cormacx.deathlink.deathlinkservice.model.death.Death;
import com.cormacx.deathlink.deathlinkservice.repository.DeathRepository;

@Service
public class DeathService {

	@Autowired
	private DeathRepository deathRepo;
	
	public Page<Death> findAllDeaths(Pageable pageable) {
		return deathRepo.findAll(pageable);
	}
	
	public Optional<Death> findDeathById( Long id ) {
		return deathRepo.findById(id);
	}
	
	
}
