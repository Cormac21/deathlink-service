package com.cormacx.deathlink.deathlinkservice.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cormacx.deathlink.deathlinkservice.model.user.Role;
import com.cormacx.deathlink.deathlinkservice.model.user.User;
import com.cormacx.deathlink.deathlinkservice.model.user.UserPrincipal;
import com.cormacx.deathlink.deathlinkservice.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	UserRepository userRepo;
	
	@Override
	public UserDetails loadUserByUsername(String username) {
		Optional<User> user = userRepo.findByEmail(username);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException("Unknown User");
        }
        return new UserPrincipal(user.get(), getAuthorities(user.get().getRoles()));
	}

	private List<GrantedAuthority> getAuthorities(List<Role> list) {
        return list.stream()
                .map(r -> new SimpleGrantedAuthority(r.getName()))
                .collect(Collectors.toList());
    }

}
