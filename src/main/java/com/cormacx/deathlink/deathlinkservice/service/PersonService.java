package com.cormacx.deathlink.deathlinkservice.service;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cormacx.deathlink.deathlinkservice.exception.DuplicatePersonException;
import com.cormacx.deathlink.deathlinkservice.model.person.Person;
import com.cormacx.deathlink.deathlinkservice.repository.PersonRepository;

@Service
public class PersonService {
	
	@Autowired
	private PersonRepository personRepo;

	public Person savePerson( Person person ) {
		if (validatePersonObject(person)) {
			if ( personExists(person)) {
				throw new DuplicatePersonException("Duplicate person found, aborting saving process!");
			} else {
				return personRepo.save(person);
			}
		} else {
			throw new IllegalArgumentException("Invalid person object!");
		}
	}
	
	public boolean personExists(Person person) {
		List<Person> preExistingPersonsWithSameName = personRepo.findAllByName(person.getName());
		if( preExistingPersonsWithSameName.isEmpty()) {
			return false;	
		} else {
			for (Person preExistingPerson : preExistingPersonsWithSameName) {
				//TODO - Consertar condi��o pra uma pessoa ser igual a outra (nome + data nasc + nome mae + nome pai)
				if (preExistingPerson.equals(person)) {
					return true;
				}
			}
			return false;
		}
	}

	private Boolean validatePersonObject(Person person) {
		return !StringUtils.isBlank(person.getName());
	}

	public Optional<Person> findPersonById(Long id) {
		return personRepo.findById(id);
	}
	
}
