package com.cormacx.deathlink.deathlinkservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cormacx.deathlink.deathlinkservice.model.Contact;
import com.cormacx.deathlink.deathlinkservice.repository.ContactRepository;

@Service
public class ContactService {
	
	@Autowired
	private ContactRepository contactRepo;
	
	public List<Contact> findContactsForUser( String username ) {
		return contactRepo.findAllByUserEmail(username);
	}

	public Contact saveContactForUser(Contact contact) {
		return contactRepo.save(contact);
	}

	public void removeContact(Contact contact) {
		contactRepo.delete(contact);
	}
	
	public void removeContactById(Long contactId) {
		contactRepo.deleteById(contactId);
	}

	public Optional<Contact> findContactByIdAndUser(Long userId, Long id) {
		return contactRepo.findOptionalByIdAndUserId(id, userId);
	}
	
}
